package com.abhisek.Second;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.client.RestTemplate;
import org.springframework.web.bind.annotation.RestController;

//import com.abhisek.LoginMs.model.User;
import com.abhisek.Second.Model.UserRepos;
import com.abhisek.Second.Model.Users;


@RestController
public class SecondRegistrationController {

	
	@Autowired
	private UserRepos userRepo;
	
	
	@RequestMapping("/check")
	public String registerUser() {
		
		return "registratration check";
	}

	@RequestMapping("/register-user/{uId}/{userName}/{password}")
	public String registerUser(@PathVariable("uId") Integer uId, @PathVariable("userName") String userName, @PathVariable("password") String password) {
		
		Users u=new Users();
		u.setId(uId);
		u.setName(userName);
		u.setPassword(password);
		
		userRepo.save(u);
		
		return "User Successfully registered";
	}
	
	
//	@PostMapping(value="/check-user")
//	public Boolean checkUser(@RequestParam("uid") int uid, @RequestParam("name") String name,@RequestParam("password") String password) {
//		Users user=userRepo.findById(uid).orElse(null);
//		if (user==null) {
//			return false;
//		}
//		else {
//			if (password.equals(user.getPassword())) {
//				return true;
//			}
//			else {
//				return false;
//			}
//		}
//	}
	/*@PostMapping(value="register-user")
	public Boolean addData(Users user) {
		try {
			System.out.println(user.toString());
			userRepo.save(user);
			return true;
		} catch(Exception e) {
			return false;
		}
	}
	*/
	
}
