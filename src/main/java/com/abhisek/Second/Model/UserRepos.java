package com.abhisek.Second.Model;

import org.springframework.data.jpa.repository.JpaRepository;



public interface UserRepos extends JpaRepository<Users, Integer>{
Users findByName(String userName);
	
	boolean findById(int userId);

	//boolean findByPassword(String password);

	//boolean findByPassword(String passWord);

	//String findByPassword(String password);

}
