package com.abhisek.Second;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class SecondTcsRegisterMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecondTcsRegisterMsApplication.class, args);
	}

}
